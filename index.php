<!DOCTYPE html>
<?php
include 'logic/fetchMenu.php';

include 'logic/counter.php';
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BSH.e9a.at</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <?php include 'logic/createNavbar.php' ?>

    <div class="row">
        <div id="thisWeekPDF" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

            <?php
                clearstatcache();
                if(filesize('menu/thisWeekMenu.pdf') == 0) {
                    echo '
                    <div class="jumbotron">
                      <h1Hi</h1>
                      <p>Wir entschuldigen uns für die Unannehmlichkeiten, doch es scheint so als würde der Speiseplan 
                        von dieser Woche nicht online verfügbar sein. In diesem Fall sprich am besten jemanden des 
                        Küchenpersonals darauf an oder warte noch ein paar Tage.</p>
                        <p><a class="btn btn-primary btn-lg" href="http://www.bshkrems.at/download/Speiseplan_a.pdf" role="button" target="_blank">Speiseplan manuell aufrufen</a></p>
                    </div>
                    ';
                } else {
                    echo '
                    <object class="pdf_object" data="menu/thisWeekMenu.pdf" type="application/pdf">
                        <iframe class="pdf_loader_object" src="https://docs.google.com/gview?embedded=true&url=https://bsh.e9a.at/menu/thisWeekMenu.pdf"
                                frameborder="0">
                        </iframe>
                    </object>
                    ';
                }
            ?>

        </div>

        <div id="nextWeekPDF" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?php
                if(filesize('menu/nextWeekMenu.pdf') == 0) {
                    echo '
                    <div class="jumbotron">
                      <h1Hi</h1>
                      <p>Wir entschuldigen uns für die Unannehmlichkeiten, doch es scheint so als würde der Speiseplan 
                        von der nächsten Woche nicht online verfügbar sein. In diesem Fall sprich am besten jemanden des 
                        Küchenpersonals darauf an oder warte noch ein paar Tage.</p>
                        <p><a class="btn btn-primary btn-lg" href="http://www.bshkrems.at/download/Speiseplan_b.pdf" role="button" target="_blank">Speiseplan manuell aufrufen</a></p>
                    </div>
                    ';
                } else {
                    echo '
                    <object class="pdf_object" data="menu/nextWeekMenu.pdf" type="application/pdf">
                        <iframe class="pdf_loader_object" src="https://docs.google.com/gview?embedded=true&url=https://bsh.e9a.at/menu/nextWeekMenu.pdf"
                                frameborder="0">
                        </iframe>
                    </object>
                    ';
                }
            ?>
        </div>
    </div>
</div>

<?php include 'logic/scripts.php' ?>
<script>
    function onClickScrollToNextWeekPDF() {
        $('html, body').animate({
            scrollTop: $("#nextWeekPDF").offset().top
        }, 1000);
    }
</script>

</body>
</html>
