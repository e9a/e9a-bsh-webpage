<?php
/**
 * Created by PhpStorm.
 * User: dafnik
 * Date: 11.12.17
 * Time: 14:05
 */
?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Navigation umschalten</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">BSH.e9a.at</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Speisepläne<span class="sr-only">(current)</span></a></li>
        <li class="hidden-md hidden-lg">
          <button type="button" style="margin-left: 12px;" class="btn btn-primary navbar-btn"
                  onclick="onClickScrollToNextWeekPDF()">
            Nächste Woche
          </button>
        </li>

        <li>
          <button type="button" style="margin-left: 12px;" class="btn btn-primary navbar-btn"
                  data-toggle="modal" data-target="#modal_telegram">
            Tägliche Essensaussendung
          </button>
        </li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <button type="button" style="margin-left: 12px;" class="btn btn-info navbar-btn"
                  data-toggle="modal" data-target="#modal_information">
            Informationen
          </button>
        </li>
        <li><a href="https://e9a.at">e9a.at</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="row" id="hello_alert"></div>

<!-- Modal -->
<div id="modal_information" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Informationen</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-default">
          <div class="panel-heading">Bibliotheken</div>

          <ul class="list-group">
            <li class="list-group-item"><a target="_blank" href="https://getbootstrap.com/">Bootstrap v3</a></li>
            <li class="list-group-item"><a target="_blank" href="https://jquery.com/">jQuery</a></li>
            <li class="list-group-item"><a target="_blank" href="https://www.google.com/intl/de_at/docs/about/">Google
                Docs als PDF Viewer für mobile Geräte</a></li>
          </ul>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">Mitwirkende</div>

          <ul class="list-group">
            <li class="list-group-item">Idee & Entwicklung der Beta: <a target="_blank" href="https://https://e9a.at/">Bernhard
                Steindl</a></li>
            <li class="list-group-item">Aktueller Entwickler: <a target="_blank" href="https://dafnik.me/">Dominik
                Dafert</a></li>
          </ul>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">Lizenz: GPLv3</div>

          <ul class="list-group">
            <li class="list-group-item">VERSION 1.4-20180502-final</li>
            <li class="list-group-item">Source Code: <a target="_blank"
                                                        href="https://gitlab.com/hniestl/e9a-bsh-webpage/">GitLab</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="modal_telegram" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Essensaussendung</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">Telegram Bot</div>
              <div class="panel-body">
                Bist du gelangweilt davon jeden Tag nachschauen zu müssen, welches Essen es diesen Tag im
                Bundesschülerheim Krems
                gibt? Ja? Dann bist du hier richtig. Extra für diese Fälle hat einer junger Hengst namens
                <a href="https://telegram.me/OxEAB" target="_blank">Elias Albertus Baterker</a>
                einen Telegram-Bot entwickelt welcher dir täglich die Speisen des heutigen Tages schickt.
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <a target="_blank" type="button" class="btn btn-primary" style="float: left" href="https://telegram.org/apps">Telegram
            downloaden</a>
          <a target="_blank" type="button" class="btn btn-success" style="float: left"
             href="https://telegram.me/haunsbot">Hier zum Bot</a>
          <button type="button" class="btn btn-danger right" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
</div>
